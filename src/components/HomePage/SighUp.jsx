import React, { useRef, useState } from "react";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { BsPlusCircleFill } from "react-icons/Bs";
import { useDispatch } from "react-redux";
import { setSignUp } from "../../store/homeSlice";
import Spinner from "react-bootstrap/Spinner";
import ToastContainer from "react-bootstrap/ToastContainer";
import Error from "./Error";
import { useSighUpOrLogInUserMutation } from "../../services/appApi";
import { useNavigate } from "react-router-dom";

const SighUp = () => {
  const dispatch = useDispatch();
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const nameRef = useRef(null);
  const navigate = useNavigate();
  const [image, setImage] = useState(null);
  const [imgUploading, setimgUploading] = useState(false);
  const [previewImg, setPreviewImg] = useState(null);
  const [errorArr, setErrorArr] = useState([]);
  const [signupUser, { isLoading, error }] = useSighUpOrLogInUserMutation();

  const defaultPic =
    "https://media.discordapp.net/attachments/764341467942354975/1118397683133055026/defaultpic.png?width=720&height=720";

  function validateImg(e) {
    let file = e.target.files[0];
    if (file) {
      if (file.size >= 1048576) {
        setErrorArr([...errorArr, { errorMsg: "Max file size is 1MB" }]);
      } else {
        setImage(file);
        setPreviewImg(URL.createObjectURL(file));
      }
    }
  }

  function handleAlreadyAccount() {
    dispatch(setSignUp());
  }

  async function uploadImage(image) {
    const data = new FormData();
    data.append("file", image);
    data.append("upload_preset", "chatapp");
    try {
      let res = await fetch(
        "https://api.cloudinary.com/v1_1/dbcabuowa/image/upload",
        {
          method: "post",
          body: data,
        }
      );
      const urldata = await res.json();
      setimgUploading(false);
      return urldata.url;
    } catch (error) {
      setimgUploading(false);
      console.log("Error while uploading image", error);
    }
  }

  async function handleSubmit(event) {
    event.preventDefault();
    const name = nameRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    if (name == "")
      setErrorArr([...errorArr, { errorMsg: "Please enter your name" }]);
    else if (email == "")
      setErrorArr([...errorArr, { errorMsg: "Please enter your email" }]);
    else if (password == "")
      setErrorArr([...errorArr, { errorMsg: "Please enter your password" }]);
    else if (!image)
      setErrorArr([...errorArr, { errorMsg: "Please upload your image" }]);
    else {
      setimgUploading(true);
      let url = await uploadImage(image);
      signupUser({ name, email, password, picture: url }).then(({ data }) => {
        if (data) {
          navigate("/chat");
        }
      });
    }
    setTimeout(() => setErrorArr([]), 2000);
  }

  return (
    <>
      <div className="form-container sign-up-container">
        <form className="form" action="#">
          <h1>Create Account</h1>
          <div className="profile-pic-container">
            <img
              src={previewImg || defaultPic}
              className="signup-profile-pic"
            />
            <label
              htmlFor="img-upload"
              className="fas fa-plus-circle image-upload-container"
            >
              <i className="add-pic-icon">
                <BsPlusCircleFill />
              </i>
            </label>
            <input
              type="file"
              id="img-upload"
              hidden
              accept="image/png, image/jpeg"
              onChange={validateImg}
            />
          </div>
          <FloatingLabel
            controlId="floatingInput1"
            label="Name"
            className="mb-3"
          >
            <Form.Control
              ref={nameRef}
              type="text"
              placeholder="name@example.com"
            />
          </FloatingLabel>
          <FloatingLabel
            controlId="floatingInput2"
            label="Email"
            className="mb-3"
          >
            <Form.Control
              ref={emailRef}
              type="email"
              placeholder="name@example.com"
            />
          </FloatingLabel>
          <FloatingLabel
            controlId="floatingInput3"
            label="Password"
            className="mb-3"
          >
            <Form.Control
              ref={passwordRef}
              type="password"
              placeholder="name@example.com"
            />
          </FloatingLabel>
          <a href="#" onClick={handleAlreadyAccount}>
            Already have a account ?
          </a>
          <button onClick={handleSubmit}>
            {" "}
            {imgUploading ? <Spinner animation="border" /> : "Sign Up"}
          </button>
        </form>
      </div>

      <ToastContainer
        className="p-3"
        position="top-center"
        style={{ zIndex: 100 }}
      >
        {errorArr.map((errorItem, idx) => (
          <Error key={idx} errorMsg={errorItem.errorMsg} />
        ))}
      </ToastContainer>
    </>
  );
};

export default SighUp;
