import { createSlice } from "@reduxjs/toolkit";
import appApi from "../services/appApi";

export const userSlice = createSlice({
  name: "user",
  initialState: null,
  reducers: {
    addNotifications: (state, { payload }) => {
      const userState = getState().user;/// change state
      console.log(payload, "slice");
      console.log(userState, "state");
      console.log(userState.newMessages, "new messages");
      if (userState.newMessages[payload]) {
        userState.newMessages[payload] = userState.newMessages[payload] + 1;
      } else {
        userState.newMessages[payload] = 1;
      }
    },
    resetNotifications: (state, { payload }) => {
      // delete state.newMessages[payload];
    },
    setUserStatus: (state, action) => {
      state.status = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      appApi.endpoints.sighUpOrLogInUser.matchFulfilled,
      (state, { payload }) => payload
    );

    builder.addMatcher(appApi.endpoints.LogOutUser.matchFulfilled, () => null);
  },
});

export const { addNotifications, resetNotifications, setUserStatus } =
  userSlice.actions;
export default userSlice.reducer;
