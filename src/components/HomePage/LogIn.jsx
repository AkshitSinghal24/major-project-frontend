import React, { useContext, useRef, useState } from "react";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { useDispatch } from "react-redux";
import { setSignUp } from "../../store/homeSlice";
import ToastContainer from "react-bootstrap/ToastContainer";
import Error from "./Error";
import { useSighUpOrLogInUserMutation } from "../../services/appApi";
import { useNavigate } from "react-router-dom";
import { AppContext } from "../../context/appContext";
import Spinner from "react-bootstrap/Spinner";

const LogIn = () => {
  const dispatch = useDispatch();
  const [errorArr, setErrorArr] = useState([]);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const [logInUser, { isLoading, error }] = useSighUpOrLogInUserMutation();
  const { socket } = useContext(AppContext);
  const navigate = useNavigate();
  const [logInLoading, setLogInLoading] = useState(false);

  function handleSubmit(event) {
    event.preventDefault();
    setLogInLoading(true);
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    if (email == "")
      setErrorArr([...errorArr, { errorMsg: "Please enter your email" }]);
    else if (password == "")
      setErrorArr([...errorArr, { errorMsg: "Please enter your password" }]);
    else {
      logInUser({ email, password })
        .then((res) => {
          setLogInLoading(false);
          if (res.data) {
            socket.emit("new-user");
            navigate("/chat");
          } else {
            setErrorArr([...errorArr, { errorMsg: res.error.data.error }]);
          }
        })
        .catch((err) => {
          console.log("Error while trying to login");
        });
    }
    setTimeout(() => setErrorArr([]), 2000);
  }

  function handleNewAccount() {
    dispatch(setSignUp());
  }

  return (
    <>
      <div className="form-container sign-in-container">
        <form className="form" action="#">
          <h1>Welcome Back!</h1>
          <div className="profile-pic-container">
            <img src="https://media.discordapp.net/attachments/764341467942354975/1118397683133055026/defaultpic.png?width=720&height=720" />
          </div>
          <FloatingLabel
            controlId="floatingInput1"
            label="Email"
            className="mb-3"
          >
            <Form.Control
              ref={emailRef}
              type="email"
              placeholder="name@example.com"
            />
          </FloatingLabel>
          <FloatingLabel
            controlId="floatingInput2"
            label="Password"
            className="mb-3"
          >
            <Form.Control
              ref={passwordRef}
              type="password"
              placeholder="name@example.com"
            />
          </FloatingLabel>
          <a href="#" onClick={handleNewAccount}>
            Create a new account ?
          </a>
          <button onClick={handleSubmit}>
            {!logInLoading ? "Log In" : <Spinner animation="border" />}
          </button>
        </form>
      </div>

      <ToastContainer
        className="p-3"
        position="top-center"
        style={{ zIndex: 100 }}
      >
        {errorArr.map((errorItem, idx) => (
          <Error key={idx} errorMsg={errorItem.errorMsg} />
        ))}
      </ToastContainer>
    </>
  );
};

export default LogIn;
