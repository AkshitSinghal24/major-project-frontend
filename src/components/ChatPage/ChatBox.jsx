import React, { useContext, useEffect, useRef, useState } from "react";
import { HiOutlinePaperAirplane } from "react-icons/Hi";
import { useSelector } from "react-redux";
import { AppContext } from "../../context/appContext";

const ChatBox = () => {
  const [message, setMessage] = useState("");
  const user = useSelector((state) => state.user);
  const { socket, currentRoom, setMessages, messages, privateMemberMsg } =
    useContext(AppContext);
  const messageEndRef = useRef(null);
  useEffect(() => {
    // scrollToBottom();
  }, [messages]);

  function getFormattedDate() {
    const date = new Date();
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : "0" + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : "0" + day;
    return day + "/" + month + "/" + year;
  }

  socket.off("room-messages").on("room-messages", (roomMessages) => {
    setMessages(roomMessages);
  });

  function handleSubmit(e) {
    e.preventDefault();
    if (!message) return;

    const todayDate = getFormattedDate();
    const today = new Date();
    const minutes =
      today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    const time = today.getHours() + ":" + minutes;
    const roomId = currentRoom;
    socket.emit("message-room", roomId, message, user, time, todayDate);
    setMessage("");
  }

  function handleSubmitMessage(e) {
    setMessage(e.target.value);
  }

  return (
    <div className="chat-area">
      <div className="header chatbox-header">
        <h6>First Channel</h6>
      </div>
      <div className="chat-input-box">
        <div className="chat-box">
          {messages.map(({ _id: date, messageByDate }, idx) => {
            return (
              <div key={idx} className="chat-box-element">
                <div className="chat-box-date-container">
                  <hr className="line" />
                  <p className="chat-box-date">{date}</p>
                  <hr className="line" />
                </div>

                {messageByDate?.map(
                  ({ content, time, from: sender }, msgIdx) => {
                    return (
                      <div className={"message"} key={msgIdx}>
                        <div className="message-inner">
                          <div className="chat-box-img">
                            <img
                              src={sender.data.picture}
                              className="avatar-user"
                            />
                          </div>
                          <div className="chat-box-text">
                            <div className="chat-box-upper">
                              <p className="message-sender">
                                {sender.data._id === user?.data._id
                                  ? "You"
                                  : sender.data.name}
                              </p>
                              <p className="message-timestamp">{time}</p>
                            </div>

                            <p className="message-content">{content}</p>
                          </div>
                        </div>
                      </div>
                    );
                  }
                )}
              </div>
            );
          })}
        </div>
        <input
          placeholder="Type your message here"
          id="chat-input"
          onChange={handleSubmitMessage}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              handleSubmit(e);
            }
          }}
        />
        <span id="chat-sendicon" onClick={handleSubmit}>
          <HiOutlinePaperAirplane id="send-icon" />
        </span>
      </div>
    </div>
  );
};

export default ChatBox;
