import React, { useContext } from "react";
import { IconContext } from "react-icons";
import { FaUser } from "react-icons/fa";
import { AppContext } from "../../context/appContext";
import { useDispatch, useSelector } from "react-redux";
import { resetNotifications, addNotifications } from "../../store/userSlice";

const generateInitials = (name) => {
  const words = name.split(" ");
  if (words.length === 1) {
    return name.charAt(0);
  } else {
    return words[0].charAt(0) + words[words.length - 1].charAt(0);
  }
};

const Channel = ({ name, joinRoom }) => {
  const initials = generateInitials(name);

  const {
    socket,
    setMembers,
    members,
    setCurrentRoom,
    setRooms,
    rooms,
    privateMemberMsg,
    setPrivateMemberMsg,
    currentRoom,
  } = useContext(AppContext);

  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  socket.off("notifications").on("notifications", (name) => {
    if (currentRoom !== name) dispatch(addNotifications(name));
  });

  return (
    <div
      className={currentRoom === name ? "Channel-selected" : "Channel"}
      onClick={() => joinRoom(name, true)}
      active={name == currentRoom}
    >
      <IconContext.Provider value={{ size: "1.5em" }}>
        {initials ? (
          <div className="channels-container">
            <div className="channel-icon">{initials}</div>
            <h5>{name}</h5>
          </div>
        ) : (
          <FaUser />
        )}
      </IconContext.Provider>
      {currentRoom !== name && (
        <span className="badge rounded-pill bg-primary">
          {console.log(user.data.newMessages[name])}
          {user.data.newMessages[name]}
        </span>
      )}
    </div>
  );
};

export default Channel;
