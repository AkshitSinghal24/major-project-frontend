import React, { useContext, useEffect, useState } from "react";
import { TbChevronLeft } from "react-icons/Tb";
import { AiOutlineSearch } from "react-icons/Ai";
import Channel from "./Channel";
import UserInfo from "./UserInfo";
import { AppContext } from "../../context/appContext";
import Members from "./members";
import { useDispatch, useSelector } from "react-redux";
import { addNotifications, resetNotifications } from "../../store/userSlice";

const SideBar = () => {
  const [isRoomSelected, setIsRoomSelected] = useState(true);
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const {
    socket,
    setMembers,
    members,
    setCurrentRoom,
    setRooms,
    rooms,
    privateMemberMsg,
    setPrivateMemberMsg,
    currentRoom,
  } = useContext(AppContext);

  useEffect(() => {
    if (user) {
      setCurrentRoom("general");
      getRooms();
      socket.emit("join-room", "general");
      socket.emit("new-user");
    }
  }, []);

  function getRooms() {
    fetch("http://localhost:5001/rooms")
      .then((res) => res.json())
      .then((data) => setRooms(data));
  }

  socket.off("new-user").on("new-user", (payload) => {
    setMembers(payload);
  });

  function handleExitRoom() {
    setIsRoomSelected(!isRoomSelected);
  }

  function joinRoom(name, isPublic = true) {
    socket.emit("join-room", name, currentRoom);
    setCurrentRoom(name);

    if (isPublic) {
      setPrivateMemberMsg(null);
    }
    dispatch(resetNotifications(name));
  }

  return (
    <div className="chat-utilis">
      <div className="header sidebar-header">
        {isRoomSelected ? (
          <TbChevronLeft onClick={handleExitRoom} id="arrow" />
        ) : (
          <pre> </pre>
        )}
        <h6> {isRoomSelected ? "All channels" : `Channel`}</h6>
      </div>
      {!isRoomSelected && (
        <>
          <div className="search-container">
            <AiOutlineSearch id="search-icon" />
            <input id="search-bar" type="text" placeholder="Search" />
          </div>
          <div className="all-channels">
            {rooms?.map((roomName) => (
              <Channel joinRoom={joinRoom} key={roomName} name={roomName} />
            ))}
          </div>
        </>
      )}

      {isRoomSelected && (
        <>
          <div className="channel-info">
            <h1 className="channel-name">Gluten</h1>
            <p className="channel-desc">
              this channel is for only those who cannot digest gluten eg. Rahul
            </p>
          </div>
          <div className="all-members">
            <h1 className="channel-name">Members</h1>
            {members.map((member) => {
              return (
                <Members joinRoom={joinRoom} key={member._id} member={member} />
              );
            })}
          </div>
        </>
      )}
      <UserInfo />
    </div>
  );
};

export default SideBar;
