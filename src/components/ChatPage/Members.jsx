import React, { useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppContext } from "../../context/appContext";
import { addNotifications, resetNotifications } from "../../store/userSlice";

const Members = ({ member, joinRoom }) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const {
    socket,
    setMembers,
    members,
    setCurrentRoom,
    setRooms,
    rooms,
    privateMemberMsg,
    setPrivateMemberMsg,
    currentRoom,
  } = useContext(AppContext);
  
  function handlePrivateMemberMsg() {
    setPrivateMemberMsg(member);
    console.log(member)
    const roomId = `${user.data._id}-${member._id}`;
    console.log(roomId);
    joinRoom(roomId, false);
  }

  return (
    <div
      onClick={handlePrivateMemberMsg}
      disabled={member._id === user._id}
      className="member-infobar"
    >
      <div className="member-info">
        <img src={member?.picture} className="avatar-user" />
        <span
          className="status"
          style={{ backgroundColor: member.status === "online" ? "green" : "red" }}
        ></span>
        <h5 id="member-name">{member?.name}</h5>
      </div>
    </div>
  );
};

export default Members;
