import React from "react";
import SideBar from "../components/ChatPage/SideBar";
import ChatBox from "../components/ChatPage/ChatBox";

const Chat = () => {
  return (
    <div className="chat">
      <SideBar />
      <ChatBox />
    </div>
  );
};

export default Chat;
