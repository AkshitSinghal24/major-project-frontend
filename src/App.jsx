import { BrowserRouter, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./pages/Home";
import Chat from "./pages/Chat";
import { useState } from "react";
import { AppContext, socket } from "./context/appContext";

function App() {
  const [rooms, setRooms] = useState([]);
  const [currentRoom, setCurrentRoom] = useState();
  const [messages, setMessages] = useState([]);
  const [members, setMembers] = useState([]);
  const [privateMembers, setPrivateMemberMsg] = useState({});
  const [newMessage, setNewMessage] = useState({});

  return (
    <AppContext.Provider
      value={{
        rooms,
        socket,
        setRooms,
        currentRoom,
        setCurrentRoom,
        messages,
        setMessages,
        members,
        setMembers,
        privateMembers,
        setPrivateMemberMsg,
        newMessage,
        setNewMessage,
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/chat" element={<Chat />} />
        </Routes>
      </BrowserRouter>
    </AppContext.Provider>
  );
}

export default App;
